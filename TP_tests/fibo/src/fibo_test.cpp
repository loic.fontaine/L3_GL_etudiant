#include "Fibo.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibo) {};
    TEST(GroupFibo, test_valeurs_0 ){
        CHECK_EQUAL(fibo(0),0);
    }

    TEST(GroupFibo, test_valeurs_1 ){
        CHECK_EQUAL(fibo(1),1);
    }

    TEST(GroupFibo, test_valeurs_2 ){
        CHECK_EQUAL(fibo(2),1);
    }

    TEST(GroupFibo, test_valeurs_3 ){
        CHECK_EQUAL(fibo(3),2);
    }

    TEST(GroupFibo, test_valeurs_4 ){
        CHECK_EQUAL(fibo(4),3);
    }

    TEST(GroupFibo, test_valeurs_5 ){
        CHECK_EQUAL(fibo(5),5);
    }
